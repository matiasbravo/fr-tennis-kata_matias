describe("Tennis Game", function() {

  it("The player 1 should have name: Chang", function() {
     var game = new Game();
     expect(game.players[0].name).toEqual('Chang');
  });

  it("The player 2 should have name: Agassi", function() {
     var game = new Game();
     expect(game.players[0].name).toEqual('Chang');
  });

  it("The player 1 should start with score 0 (love)", function() {
     var game = new Game();
     expect(game.players[0].score).toEqual('love');
  });

  it("The player 2 should start with score 0 (love)", function() {
     var game = new Game();
     expect(game.players[1].score).toEqual('love');
  });

  it("The player 1 should have score: 15 (fifteen)", function() {
     var game = new Game();
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('fifteen');
  });

  it("The player 1 should have score: 30 (thirty)", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('thirty');
  });

  it("The player 1 should have score: 40 (forty)", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(0);
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('forty');
  });

  it("The player 2 should have score: 15 (fifteen)", function() {
     var game = new Game();
     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('fifteen');
  });

  it("The player 2 should have score: 30 (thirty)", function() {
     var game = new Game();
     game.pointsTo(1);
     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('thirty');
  });

  it("The player 2 should have score: 40 (forty)", function() {
     var game = new Game();
     game.pointsTo(1);
     game.pointsTo(1);
     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('forty');
  });

 it("The player 1 should have score: 15 (fifteen) and player 2 should have score: 0 (love)", function() {
     var game = new Game();
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('fifteen');
     expect(game.pointsOf(1)).toEqual('love');
  });

  it("The player 1 should have score: 15 (fifteen) and player 2 should have score: 15 (fifteen)", function() {
     var game = new Game();
     game.pointsTo(0);

     game.pointsTo(1);

     expect(game.pointsOf(0)).toEqual('fifteen');
     expect(game.pointsOf(1)).toEqual('fifteen');
  });

  it("The player 1 should have score: 15 (fifteen) and player 2 should have score: 30 (thirty)", function() {
     var game = new Game();
     game.pointsTo(0);

     game.pointsTo(1);
     game.pointsTo(1);

     expect(game.pointsOf(0)).toEqual('fifteen');
     expect(game.pointsOf(1)).toEqual('thirty');
  });

  it("Deuce", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     expect(game.pointsOf(0)).toEqual('deuce');
     expect(game.pointsOf(1)).toEqual('deuce');
  });

  it("Advantage Chang", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('advantage Chang');
  });

  it("Advantage Chang", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('advantage Chang');
  });

  it("Advantage Agassi", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('advantage Agassi');
  });

  it("Advantage Agassi", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(1);
     game.pointsTo(0);

     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('advantage Agassi');
  });

  it("The WINNER is Chang", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(1);
     
     game.pointsTo(0);
     game.pointsTo(0);

     expect(game.pointsOf(0)).toEqual('WINNER is Chang');

  });

  it("The WINNER is Agassi", function() {
     var game = new Game();
     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(0);
     game.pointsTo(1);

     game.pointsTo(1);
     game.pointsTo(1);

     expect(game.pointsOf(1)).toEqual('WINNER is Agassi');
  });
  

});
