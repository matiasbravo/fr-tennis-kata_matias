
function Game() {

    var p1 = {
        name: 'Chang',
        score: 'love'
    }

    var p2 = {
        name: 'Agassi',
        score: 'love'
    }

    this.players = [p1, p2];

    this.pointsTo = function(player){
    
        var player = this.players[player];

        switch(player.score) {
            case 'love':
                 player.score = 'fifteen'
                break;
            case 'fifteen':
                player.score = 'thirty'
                break;
            case 'thirty':
                player.score = 'forty'
                if(this.players[0].score === 'forty' && this.players[1].score === 'forty'){
                    this.players[0].score = 'deuce';
                    this.players[1].score = 'deuce';
                }
                break;
            case 'forty':
                player.score = 'WINNER is ' + player.name
                break;
            case 'deuce':
                if(this.players[0].score === 'deuce' && this.players[1].score === 'deuce'){
                   player.score = 'advantage';
                }
                break;
             case 'advantage':
                player.score = 'WINNER is ' + player.name
                break;
        }
    };


    this.pointsOf = function(player){
        if(this.players[player].score === 'advantage') return this.players[player].score + ' ' + this.players[player].name;
        return this.players[player].score;
    };


};